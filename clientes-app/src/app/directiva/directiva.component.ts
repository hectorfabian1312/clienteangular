import { Component } from '@angular/core';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html'
})
export class DirectivaComponent {

  listaCurso: string[] = ['Java', '.Net', 'C#', 'PHP', 'JavaScript', 'TypeScript', 'Scala', 'Python', 'appScript'];
  habilitar: boolean = true;

  constructor() { }

  setHabilitar(): void {
    this.habilitar = this.habilitar ? false : true;
  }

}
