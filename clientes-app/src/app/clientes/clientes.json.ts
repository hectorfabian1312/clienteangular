import { Cliente } from './cliente';

export const CLIENTES: Cliente[] = [
  {id: 1, nombre: 'Fabian', apellido: 'Garcia', createAt: '2020-12-11', email: 'fabian@gmail.com'},
  {id: 2, nombre: 'Kilian', apellido: 'Garcia Valderrama', createAt: '2020-12-11', email: 'kilian@gmail.com'},
  {id: 3, nombre: 'Ludy', apellido: 'Valderrama Castro', createAt: '2020-12-11', email: 'ludy@gmail.com'},
  {id: 4, nombre: 'Esther', apellido: 'Alarcon', createAt: '2020-12-11', email: 'esther@gmail.com'},
  {id: 5, nombre: 'Robinson', apellido: 'Castro', createAt: '2020-12-11', email: 'robin@gmail.com'}
];
