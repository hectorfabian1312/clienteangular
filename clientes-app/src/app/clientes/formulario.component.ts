import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import swal from 'sweetalert2';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html'
})
export class FormularioComponent implements OnInit {

  public titulo: string = 'Crear Cliente';
  public cliente: Cliente = new Cliente();
  public errores: string[];

  constructor(private clienteService: ClienteService,
              private router: Router,
              private activateRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarCliente();
  }

  /* Obtenemos id del cliente e importamos  ActivatedRoute
     para pintar data al dar editar en el formulario*/
  cargarCliente(): void{
    this.activateRoute.params.subscribe(params => {
      let id = params['id'];
      if (id){
        this.clienteService.getCliente(id).subscribe((cliente) => this.cliente = cliente);
      }
    });
  }

  createCliente(): void{
    this.clienteService.createCliente(this.cliente)
    .subscribe(json => {
          this.router.navigate(['/clientes'])
          swal.fire('Cliente Guardado', `${json.mensaje}: ${json.cliente.nombre}`,  'success')
        },
        err => {
          this.errores = err.error.errors as string[];
          console.log('Codigo del error desde el backend: ' + err.status);
          console.log(err.error.errors);
        }
        );
  }

  updateCliente(): void{
    this.clienteService.updateCliente(this.cliente)
    .subscribe(json => {
          this.router.navigate(['/clientes'])
          swal.fire('Cliente Actualizado', `${json.mensaje}: ${json.cliente.nombre}`,  'success')
        },
        err => {
          this.errores = err.error.errors as string[];
          console.log('Codigo del error desde el backend: ' + err.status);
          console.log(err.error.errors);
        }
        );
  }

}
